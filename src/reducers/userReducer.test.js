import expect from 'expect';
import userReducer from './userReducer';
import * as actions from '../actions/userActions';

describe('User Reducer', () => {
  it('should add user when passed CREATE_USER_SUCCESS', () => {
    // Arrange
    const initialState = [{name: {first: 'a1', last: 'a2'}}, { name: {first: 'b1', last: 'b2'}}];
    const newUser = {name: {first: 'c1', last: 'c2'}};
    const action = actions.createUserSuccess(newUser);
    //Act
    const newState = userReducer(initialState, action);
    // Assert
    expect(newState.length).toEqual(3);
    expect(newState[0].name.first).toEqual('a1');
    expect(newState[1].name.first).toEqual('b1');
    expect(newState[2].name.first).toEqual('c1');
  });

  it('should update user when passed UPDATE_USER_SUCCESS', () => {
    // Arrange
    const initialState = [
			{guid: 1, name: {first: 'a1', last: 'a2'}}, 
			{ guid: 2, name: {first: 'b1', last: 'b2'}}
		];
    const user = {guid: 2, name: {first: 'c1', last: 'b2'}};
    const action = actions.updateUserSuccess(user);
    //Act
    const newState = userReducer(initialState, action);
    const updatedUser = newState.find(a => a.guid === user.guid);
    const untouchedCourse = newState.find(a => a.guid === '1');

    // Assert
    expect(newState[0].name.first).toEqual('a1');
    expect(newState[1].name.first).toEqual('c1');
    expect(newState[1].name.last).toEqual('b2');
    expect(newState.length).toEqual(2);
  });
});