import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function userReducer(state = initialState.users, action) {
  switch (action.type) {
    case types.LOAD_USERS_SUCCESS: {
      return action.users;
    }
    case types.CREATE_USER_SUCCESS: {
      return [
        ...state, Object.assign({}, action.user)
      ];
    }
    case types.UPDATE_USER_SUCCESS: {
      let arr = [...state];
      arr.splice(state.findIndex(user => user.guid === action.user.guid), 1, Object.assign({}, action.user));
      return [...arr];
    }
    case types.SECRET_SANTA_ASSIGNMENT_COMPLETE: {
      let arr1 = [...state];
      arr1.splice(state.findIndex(user => user.guid === action.user.guid), 1, Object.assign({}, action.user));
      return Array.from(arr1);
    }
    case types.SECRET_SANTA_AUTOASSIGN_COMPLETE: {
      return action.users;
    }
    case types.CLEAR_SECRET_SANTA_ASSIGNMENTS_COMPLETE: {
      return action.users;
    }
    default: {
      return state;
    }
  }
}