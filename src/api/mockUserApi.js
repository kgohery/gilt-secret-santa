import delay from './delay';
import db from './users';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const users = db.users; 

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (user) => {
  return user.name.first.toLowerCase() + '-' + user.name.last.toLowerCase();
};

class UserApi {
  static getAllUsers() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], users));
      }, delay);
    });
  }

  static saveUser(user) {
    user = Object.assign({}, user); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minFirstNameLength = 2;
        const minLastNameLength = 2;
        if (user.name.first.length < minFirstNameLength) {
          reject(`First name must be at least ${minFirstNameLength} characters.`);
        }

        if (user.name.last.length < minLastNameLength) {
          reject(`Last name must be at least ${minLastNameLength} characters.`);
        }

        if (user.guid) {
          const existingUserIndex = users.findIndex(a => a.id == user.guid);
          users.splice(existingUserIndex, 1, user);
        } else {
          //Just simulating creation here.
          //The server would generate ids and watchHref's for new courses in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          user.guid = generateId(user);
          users.push(user);
        }

        resolve(user);
      }, delay);
    });
  }

  // static saveAuthor(author) {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       // Simulate server-side validation
  //       const minAuthorNameLength = 3;
  //       if (author.firstName.length < minAuthorNameLength) {
  //         reject(`First Name must be at least ${minAuthorNameLength} characters.`);
  //       }

  //       if (author.lastName.length < minAuthorNameLength) {
  //         reject(`Last Name must be at least ${minAuthorNameLength} characters.`);
  //       }

  //       if (author.id) {
  //         const existingAuthorIndex = authors.findIndex(a => a.id == author.id);
  //         authors.splice(existingAuthorIndex, 1, author);
  //       } else {
  //         //Just simulating creation here.
  //         //The server would generate ids for new authors in a real app.
  //         //Cloning so copy returned is passed by value rather than by reference.
  //         author.id = generateId(author);
  //         authors.push(author);
  //       }

  //       resolve(Object.assign({}, author));
  //     }, delay);
  //   });
  // }

  // static deleteAuthor(authorId) {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       const indexOfAuthorToDelete = authors.findIndex(author => {
  //         author.authorId == authorId;
  //       });
  //       authors.splice(indexOfAuthorToDelete, 1);
  //       resolve();
  //     }, delay);
  //   });
  // }
}

export default UserApi;