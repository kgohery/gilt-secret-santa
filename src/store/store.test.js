import expect from 'expect';
import {createStore} from 'redux';
import rootReducer from '../reducers';
import initialState from '../reducers/initialState';
import * as userActions from '../actions/userActions';

describe('Store', () => {
  it('Should handle creating a user', () => {
    // arrange
    const store = createStore(rootReducer, initialState);
    const user = {
      name: {
        first: "Karl",
        last: "Gohery"
      }
    };

    // act
    const action = userActions.createUserSuccess(user);
    store.dispatch(action);

    // assert
    const actual = store.getState().users[0];
    const expected = {
      name: { first: "Karl", last: "Gohery"}
    };
    expect(actual).toEqual(expected);
  });
});