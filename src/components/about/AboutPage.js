import React from 'react';
import {Link} from 'react-router';

class AboutPage extends React.Component {
  render() {
    return (
      <div>
        <h1>About</h1>
        <p>
          This application allows you to create Secret Santa pairings for any number of people, either manually, automatically or a combination of both. Please see below for some features:   
        </p>
        <h3>Manage Secret Santa</h3>
        <ul>
          <li>Users listed automatically</li>
          <li>Select a user (giver) to see which other users are still awaiting a secret santa.</li>
          <li>On selecting a second user (recipient), a modal window is displayed to confirm the pairing. If this modal is cancelled, the modal is closed and recipient is deselected so a new recipient can be selected for the giver</li>
          <li>If the pairing is confirmed, it is saved to the store and the users are then disabled for the relevant actions i.e. a giver can't give again and a receiver can't receive twice. The pairing is added to a list at the bottom of the page where it can also be manually deleted by clicking on the pairing.</li>
          <li>All changes made here will be reflected immediately in the Users page below</li>
          <li>The page is loosely responsive i.e. the number if users / pairings displayed per row updates depending on your screensize.</li>
        </ul>
        <h3>Manage Users</h3>
        <ul>
          <li>Users can be added / updated. Loading / saving users uses a mock api to simulate the real world and some server side error checking - first / last name must be at least 2 characters long. Client side validation on email enforces the existence of "@" somewhere in the value 
          </li>
          <li>You can simulate a delay loading / saving users by updating delay.js (/src/api/delay.js) with the delay for each call in milliseconds</li>
          <li>Auto assign a secret santa recipient to each user. Note that any assignments that were made manually are maintained so it is possible to manually choose 2 pairings and then automatically assign the rest</li>
          <li>Clear all secret santa assignments</li>
        </ul>
        <p>&nbsp;</p>
        <p className="text-center">
          <Link to="/secret-santa" className="btn btn-lg btn-success" role="button">Get Started...</Link>
        </p>
      </div>
    );
  }
}

export default AboutPage;