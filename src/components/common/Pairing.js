import React, {PropTypes} from 'react';

class Pairing extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onDelete = this.onDelete.bind(this);
  }

  onDelete() {
    this.props.onDelete(this.props.user);
  }

  render() {
    return (
      <div className="pairing" onClick={this.onDelete}>
				<div className="person left">{this.props.user.name.first} {this.props.user.name.last}</div>
				<div className="person right">{this.props.user.secretSanta.name.first} {this.props.user.secretSanta.name.last}</div>
				<div className="gift"></div>
      </div>
    );
  } 
}

Pairing.propTypes = {
  user: PropTypes.object.isRequired,
	onDelete: PropTypes.func.isRequired
};

export default Pairing;