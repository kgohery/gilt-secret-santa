import React, {PropTypes} from 'react';
import Card from './Card';

class Modal extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.getGiverName = this.getGiverName.bind(this);
    this.getReceiverName = this.getReceiverName.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  getGiverName() {
    if (this.props.selectedUsers.length > 0) {
      return this.props.selectedUsers[0].name.first + " " + this.props.selectedUsers[0].name.last;
    }
    return "";
  }

  getReceiverName() {
    if (this.props.selectedUsers.length > 1) {
      return this.props.selectedUsers[1].name.first + " " + this.props.selectedUsers[1].name.last;
    }
    return "";
  }

  onChange(event) {
    this.props.onChange();
  }

  render() {
    return (
      <div className="modal-component">
        <input id="toggler" className="modal-toggler" type="checkbox" checked={this.props.display ? "checked" : ""} onChange={this.props.onChange} />
          <div className="modal-container">
            <label htmlFor="toggler" className="modal close-modal-area"></label>
            <div className="modal modal-box">
            <label htmlFor="toggler" className="close-button">X</label>
              <h3>{"So " + this.getGiverName() + " will buy for " + this.getReceiverName() + "?"}</h3>
              <div className="row">
                <div className="col-xs-6">
                  <div className="card">
                    <label className="disabled">
                      <div className="giver">
                        <div className={"avatar glyphicon glyphicon-user "}></div>
                        <div className="name">{this.getGiverName()}</div>
                      </div>
                    </label>
                  </div>
                </div>
                <div className="col-xs-6">
                  <div className="card">
                    <label className="disabled">
                      <div className="giver">
                        <div className={"avatar glyphicon glyphicon-user "}></div>
                        <div className="name">{this.getReceiverName()}</div>
                      </div>
                    </label>
                  </div>
                </div>
                <div className="col-xs-12">
                  <a href="#" onClick={this.props.onConfirm} className="toggle-modal-button">Confirm</a>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }

}

Modal.propTypes = {
  display: PropTypes.bool.isRequired,
  selectedUsers: PropTypes.array.isRequired,
  message: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Modal;