import React, {PropTypes} from 'react';
import Pairing from './Pairing';

class PairingList extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="row pairings">
        {this.props.users.map(user =>
          <div key={user.guid} className="col-xs-6 col-md-4">
            <Pairing user={user} onDelete={this.props.onDelete} />
          </div>
        )}
      </div>
    );
  } 
}

PairingList.propTypes = {
  users: PropTypes.array.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default PairingList;