import React, {PropTypes} from 'react';

class Card extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      selected: false
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    this.props.onChange(this.props.user);
  }

  render() {
    return (
      <div className="card">
        <input 
          type="checkbox" 
          id={"chk-" + this.props.user.guid} 
          name={"chk-" + this.props.user.guid}
          disabled={this.props.disabled}
          onChange={this.onChange} 
          checked={this.props.selected ? "checked" : ""} />
        <label htmlFor={"chk-" + this.props.user.guid} disabled={this.props.disabled}>
          <div className="giver">
            <div className={"avatar glyphicon glyphicon-user "}></div>
            <div className="name">{this.props.user.name.first + " " + this.props.user.name.last}</div>
          </div>
        </label>
      </div>
    );
  } 
}

Card.propTypes = {
  user: PropTypes.object.isRequired,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

export default Card;