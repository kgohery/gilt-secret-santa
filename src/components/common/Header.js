import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';

const Header = () => {
  return (
    <ul className="nav nav-tabs">
      <li role="presentation" activeClassName="active"><IndexLink to="/" activeClassName="active">Home</IndexLink></li>
      <li role="presentation"><Link to="/about" activeClassName="active">About</Link></li>
      <li role="presentation"><Link to="/secret-santa" activeClassName="active">Manage Secret Santa</Link></li>
      <li role="presentation"><Link to="/users" activeClassName="active">Manage Users</Link></li>
    </ul>
  );
};

export default Header; 