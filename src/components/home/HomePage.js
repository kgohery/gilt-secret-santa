import React from 'react';
import {Link} from 'react-router';

class HomePage extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1>Gilt Secret Santa Application</h1>

        <h4>An application for creating Secret Santa Pairings</h4>
        <p>
          <Link to="/about" className="btn btn-lg btn-success" role="button">Learn More...</Link>
        </p>
      </div>
    );
  }
}

export default HomePage;