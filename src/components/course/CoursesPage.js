// import React, {PropTypes} from 'react';
// import {connect} from 'react-redux';
// import {bindActionCreators} from 'redux';
// import * as courseActions from '../../actions/courseActions';
// import CourseList from './CourseList';
// import {browserHistory} from 'react-router';

// class CoursesPage extends React.Component {

//   constructor(props, context) {
//     super(props, context);

//     this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind(this);
//   }

//   courseRow(course, index) {
//     return <div key={index}>{course.title}</div>;
//   }

//   redirectToAddCoursePage() {
//     browserHistory.push("/course");
//   }

//   render() {
//     const {courses} = this.props;

//     return (
//       <div>
      
//         <h1>Courses</h1>
//         <input type="submit"
//                 value="Add Course"
//                 className="btn btn-primary"
//                 onClick={this.redirectToAddCoursePage} />

//         <CourseList courses={courses} />
//         <nav>
//   <ul className="pagination">
//     <li>
//       <a href="#" aria-label="Previous">
//         <span aria-hidden="true">&laquo;</span>
//       </a>
//     </li>
//     <li><a href="#">1</a></li>
//     <li><a href="#">2</a></li>
//     <li><a href="#">3</a></li>
//     <li><a href="#">4</a></li>
//     <li><a href="#">5</a></li>
//     <li>
//       <a href="#" aria-label="Next">
//         <span aria-hidden="true">&raquo;</span>
//       </a>
//     </li>
//   </ul>
// </nav>
//       </div>
//     );
//   }
// }

// CoursesPage.propTypes = {
//   courses: PropTypes.array.isRequired,
//   actions: PropTypes.object.isRequired
// };

// function mapStateToProps(state, ownProps) {
//   return {
//     courses: state.courses
//   };
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     actions: bindActionCreators(courseActions, dispatch)
//   };
// }

// export default connect(mapStateToProps, mapDispatchToProps)(CoursesPage);