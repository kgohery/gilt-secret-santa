import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const UserListRow = ({user}) => {
  return (
    <tr>
      <td><Link to={'/user/' + user.guid}>{user.name.first}</Link></td>
      <td><Link to={'/user/' + user.guid}>{user.name.last}</Link></td>
      <td>{user.secretSanta && (user.secretSanta.name.first + ' ' + user.secretSanta.name.last)}</td>
      <td>{user.email}</td>
      <td>{user.phone}</td>
    </tr>
  );
};

UserListRow.propTypes = {
  user: PropTypes.object.isRequired
};

export default UserListRow;