import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../../actions/userActions';
import UserList from './UserList';
import {browserHistory} from 'react-router';
import utils from '../../utils/utils';

class UsersPage extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.redirectToAddUserPage = this.redirectToAddUserPage.bind(this);
    this.secretSantaGenerated = this.secretSantaGenerated.bind(this);
    this.getSecretSanta = this.getSecretSanta.bind(this);
    this.generateSecretSantaAssignment = this.generateSecretSantaAssignment.bind(this);
    this.getRemainingRecipients = this.getRemainingRecipients.bind(this);
    this.getAssignedUsers = this.getAssignedUsers.bind(this);
    this.clearSecretSantaAssignments = this.clearSecretSantaAssignments.bind(this);
  }

  redirectToAddUserPage() {
    browserHistory.push("/user");
  }

  secretSantaGenerated() {
    const {users} = this.props.users;
    if (users && users.length > 0 && users[0].secretSanta) {
      return true;
    }
    return false;
  }

  getSecretSanta(currentUser, remainingUsers) {
    // Get a random number between 0 and the length of the remaining array - 1
    let assignedIndex = utils.getRandomInt(0, remainingUsers.length - 1);
    return remainingUsers[assignedIndex];
  }

  generateSecretSantaAssignment() {

    let currentUser, i, assignedUser, 
        users = Array.from(this.props.users),
        remainingRecipients = this.getRemainingRecipients(),
        unassignedUsers = [];

    // Get the users who don't yet have a secret santa assigned to them
    unassignedUsers = this.props.users.filter(user => { 
      return user.secretSanta === undefined || user.secretSanta === null;
    });

    for (i = 0; i < unassignedUsers.length; i++) {
      currentUser = Object.assign({}, unassignedUsers[i]);
      // Get the secret santa from the remaining users, excluding the current user - can't 
      // assign a user to themself 
      assignedUser = this.getSecretSanta(currentUser, remainingRecipients.filter(user => user.guid !== currentUser.guid));
      currentUser.secretSanta = Object.assign({}, assignedUser);
      // Splice the new updated user into the users array
      users.splice(users.findIndex(user => user.guid === currentUser.guid), 1, Object.assign({}, currentUser));
      // Remove the selected recipient from the remaining array
      remainingRecipients = remainingRecipients.filter(user => user.guid !== assignedUser.guid);
    }
    // Dispatch the action to update the store
    this.props.actions.secretSantaAutoAssignComplete(users);
  }

  clearSecretSantaAssignments() {
    let users = Array.from(this.props.users);
    users = users.map(user => {
      return Object.assign({}, user, {secretSanta: undefined});
    });
    // Dispatch the action
    this.props.actions.clearSecretSantaAssignmentsComplete([...users]);
  }

  getAssignedUsers() {
    // Get the users who have a secretSanta set
    return this.props.users.filter(user => {
      return user.secretSanta !== null && user.secretSanta !== undefined;
    });
  }

  getRemainingRecipients(excludeUser) {
    // Get the users who have a secretSanta set
    let assignedUsers = this.getAssignedUsers();
    // Extract an array of secretSanta recipients from these users 
    assignedUsers = assignedUsers.reduce((prev, current) => {
      return prev.concat(current.secretSanta.guid);
    }, []);
    // Get all the users who are not already assigned
    return this.props.users.filter(user => {
      // Exclude the passed user if it exists e.g. a user can't be the giver and the receiver
      if (excludeUser && excludeUser.guid === user.guid) {
        return false;
      }
      return assignedUsers.indexOf(user.guid) < 0;
    });
  }

  render() {
    const {users} = this.props;

    return (
      <div>
        <h1>Manage Users</h1>
        <nav className="navbar navbar-default navbar-static-top">
          
          <div className="container-fluid">
            <input type="submit"
                value="Add User"
                className="btn btn-primary navbar-btn"
                onClick={this.redirectToAddUserPage} />
                <div className="navbar-right">
                  <div className="btn-group" role="group">
<button
                value="Assign Secret Santa"
                className={(this.getRemainingRecipients().length > 0) ? "btn btn-primary navbar-btn" : "btn btn-primary navbar-btn disabled"}
                onClick={this.generateSecretSantaAssignment}>Assign Secret Santa</button>
        <button
                value=""
                className={(this.getAssignedUsers().length > 0) ? "btn btn-primary navbar-btn" : "btn btn-primary navbar-btn disabled"}
                onClick={this.clearSecretSantaAssignments}>Clear Secret Santa Assignments</button>
                  </div>
        
</div>
          </div>
        </nav>

        
        <UserList users={users} />
      </div>
    );
  }
}

UsersPage.propTypes = {
  users: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    users: state.users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);