import React, {PropTypes} from 'react';
import TextInput from '../common/TextInput';

const UserForm = ({user, onSave, onChange, saving, errors}) => {
  return (
    <form>
      <h1>Manage User</h1>
      <TextInput
        name="name.first"
        label="First Name"
        value={user.name.first}
        onChange={onChange}
        error={errors['name.first']} />

      <TextInput
        name="name.last"
        label="Last Name"
        value={user.name.last}
        onChange={onChange}
        error={errors['name.last']} />

      <TextInput
        name="email"
        label="Email"
        value={user.email}
        onChange={onChange}
        error={errors.email} />

      <TextInput
        name="phone"
        label="Phone"
        value={user.phone}
        onChange={onChange}
        error={errors.phone} />

      <input
        type="submit"
        disabled={saving}
        value={saving ? "Saving..." : "Save"}
        className="btn btn-primary"
        onClick={onSave} />

    </form>
  );
};

UserForm.propTypes = {
  user: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default UserForm;