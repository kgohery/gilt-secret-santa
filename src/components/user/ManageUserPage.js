import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../../actions/userActions';
import UserForm from './UserForm';
import toastr from 'toastr';

export class ManageUserPage extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      user: Object.assign({}, props.user),
      errors: {},
      saving: false
    };

    this.updateUserState = this.updateUserState.bind(this);
    this.saveUser = this.saveUser.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user.guid != nextProps.user.guid) {
      // Necessary top populate form when existing user is loaded directly
      this.setState({user: Object.assign({}, nextProps.user)});
    }
  }

  updateUserState(event) {
    const field = event.target.name;
    let user = this.state.user;
    
    // Only handles one level of sub object for now
    if (field.indexOf(".") > 0) {
      const firstProp = field.substring(0, field.indexOf("."));
      const secondProp = field.substring(field.indexOf(".") + 1);
      let obj = {};
      obj[secondProp] = event.target.value;
      user[firstProp] = Object.assign({}, user[firstProp], obj);
    } else {
      user[field] = event.target.value;
    }

    return this.setState({user: user});
  }

  userFormIsValid() {
    let formIsValid = true;
    let errors = {};

    if (this.state.user.name.first.length < 2) {
      errors['name.first'] = 'First name must be at least 2 characters.';
      formIsValid = false;
    }

    if (this.state.user.name.last.length < 2) {
      errors['name.last'] = 'Last name must be at least 2 characters.';
      formIsValid = false;
    }

    // if (this.state.user.email.indexOf("@") <= 0) {
    //   errors.email = 'Please enter a valid email address.';
    //   formIsValid = false;
    // }

    this.setState({errors: errors});
    return formIsValid;
  }

  saveUser(event) {
    event.preventDefault();

    if (!this.userFormIsValid()) {
      return;
    }
    
    this.setState({saving: true, secretSanta: (this.state.user.secretSanta || undefined)});
    this.props.actions.saveUser(this.state.user)
      .then(() => this.redirect())
      .catch(error => {
        this.setState({saving: false});
        toastr.error(error);
      });
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('User saved!');
    this.context.router.push('/users');
  }
  
  render() {
    return (
      <UserForm 
        onChange={this.updateUserState}
        onSave={this.saveUser}
        user={this.state.user}
        errors={this.state.errors}
        saving={this.state.saving} />
    );
  }
}

ManageUserPage.propTypes = {
  user: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

// Pull in the React Router context so router is available 
// on this.context.router
ManageUserPage.contextTypes = {
  router: PropTypes.object
};

function getUserByGuid(users, guid) {
  const user = users.filter(user => user.guid == guid);
  if (user.length > 0) return user[0];
  return null;
}

function mapStateToProps(state, ownProps) {
  const userGuid = ownProps.params.guid;
  let user = {guid: "", name: {first: "", last: ""}, email: "", phone: ""};
  if (userGuid && state.users.length > 0) {
    user = getUserByGuid(state.users, userGuid);
  }

  return {
    user: user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageUserPage);