import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../../actions/userActions';
import UserList from './UserList';
import Card from '../common/Card';
import Modal from '../common/Modal';
import PairingList from '../common/PairingList';
import {Link, IndexLink, browserHistory} from 'react-router';
import utils from '../../utils/utils';

export class ManageSecretSantaPage extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      selectedUsers: [],
      showModal: false
    };

    this.onCardClick = this.onCardClick.bind(this);
    this.onModalConfirmClick = this.onModalConfirmClick.bind(this);
    this.onModalChange = this.onModalChange.bind(this);
    this.cardIsDisabled = this.cardIsDisabled.bind(this);
    this.isAlreadyRecipient = this.isAlreadyRecipient.bind(this);
    this.getUserByGuid = this.getUserByGuid.bind(this);
    this.onDeleteAssignment = this.onDeleteAssignment.bind(this);
    this.getUsersFromSelectedUsers = this.getUsersFromSelectedUsers.bind(this);
    this.getAssignedUsers = this.getAssignedUsers.bind(this);
  }

  onCardClick(user, event) {
    const selectedUserIndex = this.state.selectedUsers.indexOf(user.guid);
    let arr = [...this.state.selectedUsers];
    if (selectedUserIndex >= 0) {
      arr.splice(selectedUserIndex, 1);
    } else {
      arr.push(user.guid);
    }

    this.setState({selectedUsers: Array.from(arr)});
    arr.length === 2 && this.setState({showModal: true});
  }

  onModalConfirmClick() {
    let user = Object.assign({}, this.getUserByGuid(this.state.selectedUsers[0]));
    user.secretSanta = Object.assign({}, this.getUserByGuid(this.state.selectedUsers[1]));
    this.props.actions.secretSantaAssignmentComplete(user);
    this.setState({selectedUsers: [], showModal: false});
  }

  onModalChange() {
    // If this is the 2nd item in the array i.e. the recipient, unselect is 
    // once the modal is cancelled
    if (this.state.selectedUsers.length === 2) {
      const arr = Array.from(this.state.selectedUsers);
      arr.splice(1, 1);
      this.setState({selectedUsers: arr, showModal: false});
    }
  }

  getUserByGuid(guid) {
    return this.props.users.find(user => user.guid === guid);
  }

  cardIsSelected(guid) {
    return this.state.selectedUsers.indexOf(guid) >= 0;
  }

  isAlreadyRecipient(user) {
    return this.props.users.filter(u => u.secretSanta && u.secretSanta.guid === user.guid);
  }

  onDeleteAssignment(user) {
    this.props.actions.secretSantaAssignmentComplete(Object.assign({}, user, {secretSanta: undefined}));
  }

  cardIsDisabled(user) {
    if (this.state.selectedUsers.length === 0) {
      // No giver has been selected so the only users to be disabled are 
      // users who have already been assigned their secret santa recipient
      return !(user.secretSanta === undefined || user.secretSanta === null); 
    } else if (this.state.selectedUsers.length === 1) {
      // The giver has been selected so only those who have not already been
      // selected to receive a gift are enabled
      return this.isAlreadyRecipient(user).length > 0;
    } else if (this.state.selectedUsers.length >= 2) {
      const index = this.state.selectedUsers.indexOf(user.guid);
      // You can always reselect who the receiver is so the 2nd item is always enabled.
      // Otherwise disable all other cards if 2 are selected  
      return (index !== 1);
    }
    return false;
  }

  getUsersFromSelectedUsers() {
    return this.state.selectedUsers.map(guid => {
      return this.getUserByGuid(guid);
    });
  }

  getAssignedUsers() {
    return this.props.users.filter(user => {
      return user.secretSanta !== null && user.secretSanta !== undefined;
    });
  }

  render() {
    this.getAssignedUsers();
    return (
     <div>
        <h1>Manage Secret Santa</h1>
        <Modal
          display={this.state.showModal} 
          selectedUsers={this.getUsersFromSelectedUsers()} 
          message="This is the modal"
          onConfirm={this.onModalConfirmClick}
          onChange={this.onModalChange} />
          <div className="well well-lg">
        <div className="row">

          {this.props.users.map(user =>
            <div key={user.guid} className="col-xs-6 col-sm-3 col-md-2">
              <Card 
                user={user} 
                selected={this.cardIsSelected(user.guid)}
                disabled={this.cardIsDisabled(user)}
                onChange={this.onCardClick} />
            </div>
          )}
        </div>
        </div>
        <h3>Secret Santas Pairings</h3>
        <div className="well well-lg">
          <PairingList onDelete={this.onDeleteAssignment} users={this.getAssignedUsers()} />
        </div>
      </div>
    );
  }
}

ManageSecretSantaPage.propTypes = {
  users: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  let users = state.users.sort((prev, current) => prev.name.last > current.name.last);

  return {
    users: users
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageSecretSantaPage);