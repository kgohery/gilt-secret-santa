import expect from 'expect';
import * as userActions from './userActions';
import * as types from './actionTypes';

import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store'; 

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('USER ACTIONS', () => {
  it('should create LOAD_USERS_SUCCESS when loading users', (done) => {
    const expectedActions = [
      { type: types.LOAD_USERS_SUCCESS, body: { 
				users: [{id: '123456', name: {first: 'Karl', last: 'Gohery'}}]}
			}
    ];
    const store = mockStore({users: []}, expectedActions);

    store.dispatch(userActions.loadUsers()).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(types.LOAD_USERS_SUCCESS);
      done();
    });
  });
});